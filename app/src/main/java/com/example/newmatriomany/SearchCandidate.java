package com.example.newmatriomany;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.newmatriomany.adapter.CandidateListAdapter;
import com.example.newmatriomany.database.Tbl_User;
import com.example.newmatriomany.model.Candidate;

import java.util.ArrayList;


public class SearchCandidate extends Fragment {

    //EditText For Search Element
    EditText fragment_search_candidate_etSearch;

    //Display Data Into List
    RecyclerView fragment_search_candidate_rvSearch;

    //Databse Object
    Tbl_User tbl_user;

    //Temp String For Search Element
    String data;

    //Array List Of Candidate Model Object
    ArrayList<Candidate> candidateList;

    //Adapter For Display Data Into List
    CandidateListAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = LayoutInflater.from(getContext()).inflate(R.layout.fragment_search_candidate, container, false);

        init(view);
        return view;
    }

    void init(View view) {

        //Defualt Data Is Null
        data = null;

        //Recycler View Bind With ID
        fragment_search_candidate_rvSearch = view.findViewById(R.id.fragment_search_candidate_rvSearch);

        //User Input Get From EditText
        fragment_search_candidate_etSearch = view.findViewById(R.id.fragment_search_candidate_etSearch);

        //Create Empty List
        candidateList = new ArrayList<>();

        //Database Object
        tbl_user = new Tbl_User(getContext());

        //Get Candidate List From Database
        candidateList = tbl_user.all_CandidateList();

        fragment_search_candidate_etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                data = charSequence.toString();
                setRecyclerView(data);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        setRecyclerView(data);

    }

    void setRecyclerView(String data) {

        if (data == null || data.isEmpty()) {
            //Edittext Empty That Display All Candidate
            candidateList = tbl_user.all_CandidateList();
        } else {
            //If EditText Have Data That Search Element In Database
            candidateList = tbl_user.searchCandidate(data);
        }

        //Adapter Intitalize
        adapter = new CandidateListAdapter(getContext(), candidateList);

        //Set LayoutManaager For Recycler View
        fragment_search_candidate_rvSearch.setLayoutManager(new GridLayoutManager(getContext(), 1));

        //Set Adapter Of Data Into Recycler View
        fragment_search_candidate_rvSearch.setAdapter(adapter);
    }
}