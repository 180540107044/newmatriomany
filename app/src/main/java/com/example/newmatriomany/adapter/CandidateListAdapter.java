package com.example.newmatriomany.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newmatriomany.R;
import com.example.newmatriomany.database.Tbl_User;
import com.example.newmatriomany.model.Candidate;

import java.util.ArrayList;

public class CandidateListAdapter extends RecyclerView.Adapter<CandidateListAdapter.RecyclerViewHolder> {

    Context context;
    ArrayList<Candidate> candidateList;
    Tbl_User tbl_user;

    public CandidateListAdapter(Context context, ArrayList<Candidate> candidateList) {
        this.context = context;
        this.candidateList = candidateList;
        this.tbl_user = new Tbl_User(this.context);
    }

    @NonNull
    @Override
    public CandidateListAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_candidate, parent, false);

        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {

        holder.row_list_candidate_tvName.setText(candidateList.get(position).getFirstname());
        holder.row_list_candidate_tvMobile.setText(candidateList.get(position).getPhonenumber());
        holder.row_list_candidate_tvGender.setText(candidateList.get(position).getGender());
        holder.row_list_candidate_tvEmail.setText(candidateList.get(position).getEmail());

        if (candidateList.get(position).getIsFavorite() == 0) {
            holder.row_list_candidate_ivFavorite.setImageResource(R.drawable.ic_unfavorite);
        } else {
            holder.row_list_candidate_ivFavorite.setImageResource(R.drawable.ic_favorite);
        }

        holder.row_list_candidate_ivFavorite.setOnClickListener(view -> {

            if ((candidateList.get(position).getIsFavorite() == 0 ? 1 : 0) == 0) {
                holder.row_list_candidate_ivFavorite.setImageResource(R.drawable.ic_unfavorite);
            } else {
                holder.row_list_candidate_ivFavorite.setImageResource(R.drawable.ic_favorite);
            }
            tbl_user.changeFavorite(candidateList.get(position).getIsFavorite() == 0 ? 1 : 0, candidateList.get(position).getId());
            candidateList.get(position).setIsFavorite(candidateList.get(position).getIsFavorite() == 0 ? 1 : 0);
            //notifyDataSetChanged();


        });


    }

    @Override
    public int getItemCount() {
        return candidateList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView row_list_candidate_tvName, row_list_candidate_tvMobile, row_list_candidate_tvGender, row_list_candidate_tvEmail;

        ImageView row_list_candidate_ivFavorite;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);

            row_list_candidate_tvName = itemView.findViewById(R.id.row_list_candidate_tvName);
            row_list_candidate_tvMobile = itemView.findViewById(R.id.row_list_candidate_tvMobile);
            row_list_candidate_tvGender = itemView.findViewById(R.id.row_list_candidate_tvGender);
            row_list_candidate_tvEmail = itemView.findViewById(R.id.row_list_candidate_tvEmail);

            row_list_candidate_ivFavorite = itemView.findViewById(R.id.row_list_candidate_ivFavorite);

        }
    }
}
