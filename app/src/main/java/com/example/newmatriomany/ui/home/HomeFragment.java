package com.example.newmatriomany.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newmatriomany.R;
import com.example.newmatriomany.adapter.CandidateListAdapter;
import com.example.newmatriomany.database.Tbl_User;
import com.example.newmatriomany.model.Candidate;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    int lengthCandidateList;
    CandidateListAdapter candidateListAdapter;
    ArrayList<Candidate> candidateList;
    Tbl_User tbl_user;
    RecyclerView list_candidate_rvCandidateList;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.list_candidate, container, false);
        setRecyclerView(root);
        return root;
    }

    void setRecyclerView(View view){
        //First Binf Id With Variables
        init(view);

        candidateListAdapter = new CandidateListAdapter(view.getContext(),candidateList);
        list_candidate_rvCandidateList.setLayoutManager(new GridLayoutManager(getContext(),1));
        list_candidate_rvCandidateList.setAdapter(candidateListAdapter);

    }

    //Initialize Of Data
    private void init(View view) {
        //Creating Database Object
        tbl_user = new Tbl_User(view.getContext());

        //Create Empty List
        candidateList = new ArrayList<>();

        //Get Data From Database And Set Into List
        candidateList = tbl_user.all_CandidateList();

        //Length Of CandidateList
        lengthCandidateList = candidateList.size();

        //Bind Recycler View
        list_candidate_rvCandidateList = view.findViewById(R.id.list_candidate_rvCandidateList);


    }

}
