package com.example.newmatriomany.ui.gallery;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.newmatriomany.R;
import com.example.newmatriomany.database.Tbl_User;
import com.example.newmatriomany.model.Candidate;

public class GalleryFragment extends Fragment {

    EditText efn, eln, eem, ep, erp, eph, edob;
    RadioButton rbMale, rbFemale, rbOther;
    String firstname, lastname, mail, password, confpassword, phone, dob, gender, hobbies;
    CheckBox chCricket, chChess, chVolleyBall;
    Button submit, reset;

    Tbl_User tbl_user;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        GalleryViewModel galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
        View root = inflater.inflate(R.layout.add_candidate, container, false);
        final TextView textView = root.findViewById(R.id.text_gallery);
        galleryViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        init(root);

        submit.setOnClickListener(view -> {
            afterSubmit();
            if (validate()) {


                //Create Empty Model Of Candidate
                Candidate candidate = new Candidate();

                //Set Value For Inserting Data Into Database
                candidate.setFirstname(firstname);
                candidate.setLastname(lastname);
                candidate.setPassword(password);
                candidate.setEmail(mail);
                candidate.setDob(dob);
                candidate.setPhonenumber(phone);
                candidate.setGender(gender);
                candidate.setHobbies(hobbies);
                candidate.setIsFavorite(0);

                DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
                    switch (which) {
                        //Clicked Yes Then
                        case DialogInterface.BUTTON_POSITIVE:

                            tbl_user.insert_CandidateRecord(candidate);
                            Toast.makeText(getContext(), "Inserted Success", Toast.LENGTH_SHORT).show();
                            clearData();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            //No Button Clicked
                            break;
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Are you sure insert record?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();

            }
        });
        return root;
    }

    private void clearData() {

        efn.setText("");
        eln.setText("");
        eem.setText("");
        ep.setText("");
        erp.setText("");
        eph.setText("");
        edob.setText("");

        rbMale.setChecked(false);
        rbFemale.setChecked(false);
        rbOther.setChecked(false);

        chCricket.setChecked(false);
        chChess.setChecked(false);
        chVolleyBall.setChecked(false);

    }

    private void afterSubmit() {
        firstname = efn.getText().toString().trim();
        lastname = eln.getText().toString().trim();
        mail = eem.getText().toString().trim();
        password = ep.getText().toString().trim();
        confpassword = erp.getText().toString().trim();
        phone = eph.getText().toString().trim();
        dob = edob.getText().toString().trim();

        if (rbMale.isChecked()) {
            gender = "Male";
        } else if (rbFemale.isChecked()) {
            gender = "Female";
        } else {
            gender = "Other";
        }

        hobbies = "";

        if (chCricket.isChecked()) {
            hobbies += "Cricket ";
        }
        if (chChess.isChecked()) {
            hobbies += "Chess ";
        }
        if (chVolleyBall.isChecked()) {
            hobbies += "VolleyBall ";
        }
    }

    public void init(View view) {
        efn = view.findViewById(R.id.fname);
        eln = view.findViewById(R.id.lname);
        eem = view.findViewById(R.id.uemail);

        ep = view.findViewById(R.id.pass);
        erp = view.findViewById(R.id.rpass);
        eph = view.findViewById(R.id.contact);

        edob = view.findViewById(R.id.date);

        rbMale = view.findViewById(R.id.radioButton1);
        rbFemale = view.findViewById(R.id.radioButton2);
        rbOther = view.findViewById(R.id.radioButton3);

        chCricket = view.findViewById(R.id.chCricket);
        chChess = view.findViewById(R.id.chChess);
        chVolleyBall = view.findViewById(R.id.chVolleyBall);


        submit = view.findViewById(R.id.submit);
        reset = view.findViewById(R.id.reset);

        rbFemale.setChecked(true);

        tbl_user = new Tbl_User(getContext());

    }

    public boolean validate() {

        firstname = efn.getText().toString().trim();
        lastname = eln.getText().toString().trim();
        mail = eem.getText().toString().trim();
        password = ep.getText().toString().trim();
        confpassword = erp.getText().toString().trim();
        phone = eph.getText().toString().trim();
        dob = edob.getText().toString().trim();

        boolean val = true;
        if (firstname.isEmpty()) {
            efn.setError("!! First Name cant be blank");
            efn.requestFocus();
            val = false;

            if (lastname.isEmpty()) {
                eln.setError("!! Last Name cant be blank");
                eln.requestFocus();
                val = false;
            }
            if (mail.isEmpty()) {
                eem.setError("!! Mail cant be blank");
                eem.requestFocus();
                val = false;
            }
            if (dob.isEmpty()) {
                edob.setError("!! User BirthDate cant be blank");
                edob.requestFocus();
                val = false;
            }
            if (password.isEmpty()) {
                ep.setError("!! Password cant be blank");
                ep.requestFocus();
                val = false;
            }
            if (confpassword.isEmpty()) {
                erp.setError("!! Confirm password cant be blank");
                erp.requestFocus();
                val = false;
            }
            if (phone.isEmpty()) {
                eph.setError("Enter Your Phone number");
                eph.requestFocus();
                val = false;
            }
            if (phone.length() != 10) {
                eph.setError("Enter 10 digit mobile number only");
                eph.requestFocus();
                val = false;

            }
        }
        if (mail.isEmpty()) {
            eem.setError("Please, Enter E-mail Address");
            eem.requestFocus();
            val = false;
        }

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (!mail.matches(emailPattern))
        {
            eem.setError("Please, Enter valid E-mail Address");
            eem.requestFocus();
            val = false;
        }

        if (password.length() < 8) {
            ep.setError("Minimum password length is 8");
            ep.requestFocus();
            val = false;

        }
        if (confpassword.length() < 8 || !password.equals(confpassword)) {
            erp.setError("Password doesn't match");
            erp.requestFocus();
            val = false;
        }

        return val;
    }

}
