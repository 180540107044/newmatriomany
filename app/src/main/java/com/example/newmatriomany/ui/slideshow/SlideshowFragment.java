package com.example.newmatriomany.ui.slideshow;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newmatriomany.R;
import com.example.newmatriomany.adapter.CandidateListAdapter;
import com.example.newmatriomany.database.Tbl_User;
import com.example.newmatriomany.model.Candidate;

import java.util.ArrayList;

public class SlideshowFragment extends Fragment {

    RecyclerView favorite_candidate_rvCandidateList;

    Tbl_User tbl_user;
    ArrayList<Candidate> candidateList;
    int lengthCandidateList;
    CandidateListAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.favourite_candidate, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        //Creating Database Object
        tbl_user = new Tbl_User(view.getContext());

        //Create Empty List
        candidateList = new ArrayList<>();

        //Get Data From Database And Set Into List
        candidateList = tbl_user.favorite_CandidateList();

        //Length Of CandidateList
        lengthCandidateList = candidateList.size();

        favorite_candidate_rvCandidateList = view.findViewById(R.id.favorite_candidate_rvCandidateList);

        adapter = new CandidateListAdapter(view.getContext(),candidateList);
        favorite_candidate_rvCandidateList.setLayoutManager(new GridLayoutManager(view.getContext(),1));
        favorite_candidate_rvCandidateList.setAdapter(adapter);

    }
}
