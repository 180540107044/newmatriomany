package com.example.newmatriomany.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.newmatriomany.model.Candidate;

import java.util.ArrayList;

public class Tbl_User extends Database_Helper {
    public Tbl_User(Context context) {
        super(context);
    }


    public final static String TBL_USER = "User";
    public final static String COL_ID = "Id";
    public final static String COL_FIRSTNAME = "Firstname";
    public final static String COL_LASTNAME = "Lastname";
    public final static String COL_GENDER = "Gender";
    public final static String COL_PHONENUMBER = "Phonenumber";
    public final static String COL_DOB = "DOB";
    public final static String COL_EMAIL = "Email";
    public final static String COL_PASSWORD = "Password";
    public final static String COL_HOBBIES = "Hobbies";
    public final static String COL_FAVORITE = "IsFavorite";


    /*public void insert_data(String firstname,String lastname,String Hobbies,String Gender,String Phonenumber){

    }*/

    //Insert Candidate Record Into Database
    public void insert_CandidateRecord(Candidate candidate) {
        //SQLite Database Object Initialize
        SQLiteDatabase db = this.getWritableDatabase();

        //Content Value For Insert Value Into Database Table
        ContentValues cv = new ContentValues();

        //Insert Data Into Content Value
        cv.put(COL_FIRSTNAME,candidate.getFirstname());
        cv.put(COL_LASTNAME,candidate.getLastname());
        cv.put(COL_GENDER,candidate.getGender());
        cv.put(COL_DOB,candidate.getDob());
        cv.put(COL_PHONENUMBER,candidate.getPhonenumber());
        cv.put(COL_EMAIL,candidate.getEmail());
        cv.put(COL_PASSWORD,candidate.getPassword());
        cv.put(COL_HOBBIES,candidate.getHobbies());
        cv.put(COL_FAVORITE,candidate.getIsFavorite());

        //Insert ContentValue Into Database
        db.insert(TBL_USER, null, cv);

        //Database Object Close
        db.close();

    }

    //Get Candidate Table Array List
    public ArrayList<Candidate> all_CandidateList() {
        //Initialize ArrayList
        ArrayList<Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor cursor = db.rawQuery("select * from "+TBL_USER, null);

        //Start From First Result To End Loop
        if (cursor.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Candidate candidate = new Candidate();

                //Set Values Into Objects
                candidate.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                candidate.setFirstname(cursor.getString(cursor.getColumnIndex(COL_FIRSTNAME)));
                candidate.setLastname(cursor.getString(cursor.getColumnIndex(COL_LASTNAME)));
                candidate.setGender(cursor.getString(cursor.getColumnIndex(COL_GENDER)));
                candidate.setDob(cursor.getString(cursor.getColumnIndex(COL_DOB)));
                candidate.setPhonenumber(cursor.getString(cursor.getColumnIndex(COL_PHONENUMBER)));
                candidate.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));
                candidate.setPassword(cursor.getString(cursor.getColumnIndex(COL_PASSWORD)));
                candidate.setHobbies(cursor.getString(cursor.getColumnIndex(COL_HOBBIES)));
                candidate.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_FAVORITE)));

                //Add Object Into List
                candidateArrayList.add(candidate);

            } while (cursor.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        cursor.close();

        //Return Array List
        return candidateArrayList;
    }

    //Get Favorite Candidate Table Array List
    public ArrayList<Candidate> favorite_CandidateList() {
        //Initialize ArrayList
        ArrayList<Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor cursor = db.rawQuery("select * from "+TBL_USER + " where IsFavorite = 1", null);

        //Start From First Result To End Loop
        if (cursor.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Candidate candidate = new Candidate();

                //Set Values Into Objects
                candidate.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                candidate.setFirstname(cursor.getString(cursor.getColumnIndex(COL_FIRSTNAME)));
                candidate.setLastname(cursor.getString(cursor.getColumnIndex(COL_LASTNAME)));
                candidate.setGender(cursor.getString(cursor.getColumnIndex(COL_GENDER)));
                candidate.setDob(cursor.getString(cursor.getColumnIndex(COL_DOB)));
                candidate.setPhonenumber(cursor.getString(cursor.getColumnIndex(COL_PHONENUMBER)));
                candidate.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));
                candidate.setPassword(cursor.getString(cursor.getColumnIndex(COL_PASSWORD)));
                candidate.setHobbies(cursor.getString(cursor.getColumnIndex(COL_HOBBIES)));
                candidate.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_FAVORITE)));

                //Add Object Into List
                candidateArrayList.add(candidate);

            } while (cursor.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        cursor.close();

        //Return Array List
        return candidateArrayList;
    }

    public void deleteAllCandidate(){
        SQLiteDatabase db = getWritableDatabase();

        String query = "delete from " + TBL_USER;

        db.execSQL(query);

        db.close();

    }

    public void changeFavorite(int favorite,int id){
        SQLiteDatabase db = getWritableDatabase();

        String query = "Update "+TBL_USER + "  Set IsFavorite = "+favorite + " where id = "+id;

        db.execSQL(query);

        db.close();

    }

    //SearchCandidate
    public ArrayList<Candidate> searchCandidate(String data){
        //Initialize ArrayList
        ArrayList<Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor cursor = db.rawQuery("select * from "+TBL_USER + " where "+COL_FIRSTNAME+" LIKE '%"+data+"%' or "+COL_LASTNAME + "  LIKE '%"+data+"%';", null);

        //Start From First Result To End Loop
        if (cursor.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Candidate candidate = new Candidate();

                //Set Values Into Objects
                candidate.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
                candidate.setFirstname(cursor.getString(cursor.getColumnIndex(COL_FIRSTNAME)));
                candidate.setLastname(cursor.getString(cursor.getColumnIndex(COL_LASTNAME)));
                candidate.setGender(cursor.getString(cursor.getColumnIndex(COL_GENDER)));
                candidate.setDob(cursor.getString(cursor.getColumnIndex(COL_DOB)));
                candidate.setPhonenumber(cursor.getString(cursor.getColumnIndex(COL_PHONENUMBER)));
                candidate.setEmail(cursor.getString(cursor.getColumnIndex(COL_EMAIL)));
                candidate.setPassword(cursor.getString(cursor.getColumnIndex(COL_PASSWORD)));
                candidate.setHobbies(cursor.getString(cursor.getColumnIndex(COL_HOBBIES)));
                candidate.setIsFavorite(cursor.getInt(cursor.getColumnIndex(COL_FAVORITE)));

                //Add Object Into List
                candidateArrayList.add(candidate);

            } while (cursor.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        cursor.close();

        //Return Array List
        return candidateArrayList;
    }
}
