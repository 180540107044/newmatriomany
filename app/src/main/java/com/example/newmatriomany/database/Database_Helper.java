package com.example.newmatriomany.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class Database_Helper extends SQLiteAssetHelper {

    public final static int DATABASE_VERSION = 1;
    public final static String DATABASE_NAME = "Matrimony.db";

    public Database_Helper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
